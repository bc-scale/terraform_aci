README
------

Copyright (c) 2020 World Wide Technology, LLC
All rights reserved.

author: Joel W. King  @joelwking

Each module should contain a README file to describe the functionality of the module. [Standard Module Structure](https://www.terraform.io/docs/modules/index.html#standard-module-structure)


```
            ├── modules
            │   └── lldp
            │       ├── examples
            │       │   └── README.md
            │       ├── LICENSE
            │       ├── main.tf             
            │       ├── outputs.tf
            │       ├── README.md            <- You are here
            │       ├── variables.tf
            │       └── versions.tf
```


This sub-module manages [ACI LLDP Interface Policy](https://registry.terraform.io/providers/CiscoDevNet/aci/latest/docs/resources/lldpifpol)